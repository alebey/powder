import React from 'react';
import { mount } from 'enzyme';

import { email } from 'mocks/constants';
import AccountDetailsStep0 from 'views/accountDetails/step0';

const getRootComponent = () => mount(<AccountDetailsStep0 email={email} />);

describe('<AccountDetailsStep0 />', () => {
  it('renders properly', () => {
    const root = getRootComponent();
    expect(root.find('Grid[item=true]').text()).toBe(email);
  });
});
