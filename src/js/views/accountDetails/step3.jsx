import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Typography from '@material-ui/core/Typography';

import i18n from 'i18n';

export default class AccountDetailsStep3 extends PureComponent {
  static propTypes = {
    language: PropTypes.string.isRequired
  }

  render() {
    const { language } = this.props;
    const { accountDetails: { step3Content } } = i18n[language];
    return (
      <Grid container align='center' alignContent='center' spacing={24}>
        {step3Content.map(text => (
          <Grid key={text} item xs={12}>
            <Typography variant='h5'>{text}</Typography>
          </Grid>
        ))}
      </Grid>
    );
  }
}
