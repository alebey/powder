import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import i18n from 'i18n';

export default class AccountDetailsStep2 extends PureComponent {
  static propTypes = {
    language: PropTypes.string.isRequired
  }

  render() {
    const { language } = this.props;
    const { accountDetails: { step2Content } } = i18n[language];
    return (
      <Grid container alignItems='center' justify='center'>
        <Grid item>{step2Content}</Grid>
      </Grid>
    );
  }
}
